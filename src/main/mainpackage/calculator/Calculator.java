package mainpackage.calculator;

public class Calculator {

    public Calculator() {

    }

    public static String[] mixSmallLabelAndBigLabelToArray(String indexZeroValue, String[] restArray) {
        String[] finalArray = new String[restArray.length + 1];
        finalArray[0] = indexZeroValue;
        for (int i = 1; i < restArray.length + 1; i++) {
            finalArray[i] = restArray[i - 1];
        }
        return finalArray;
    }

    public static String computeResult(String[] array) {
        computeMultiply(array);
        computeDevide(array);
        computeAdd(array);
        computeSubstract(array);
        return array[0];
    }


    private static void computeMultiply(String[] array) {
        for (int i = 0; i < array.length; i++) {
            if (array[i].equals("*")) {
                array[i - 1] = String.valueOf(multiply(Double.parseDouble(array[i - 1]), Double.parseDouble(array[i + 1])));
                moveArrayForward(array, i + 2);
                if (i > 1) {
                    i = i - 2;
                } else if (i == 1) {
                    i = i - 1;
                }
            }
        }
    }

    private static void computeDevide(String[] array) {
        for (int i = 0; i < array.length; i++) {
            if (array[i].equals("/")) {
                array[i - 1] = String.valueOf(devide(Double.parseDouble(array[i - 1]), Double.parseDouble(array[i + 1])));
                moveArrayForward(array, i + 2);
                if (i > 1) {
                    i = i - 2;
                } else if (i == 1) {
                    i = i - 1;
                }
            }
        }
    }

    private static void computeAdd(String[] array) {
        for (int i = 0; i < array.length; i++) {
            if (array[i].equals("+")) {
                array[i - 1] = String.valueOf(add(Double.parseDouble(array[i - 1]), Double.parseDouble(array[i + 1])));
                moveArrayForward(array, i + 2);
                if (i > 1) {
                    i = i - 2;
                } else if (i == 1) {
                    i = i - 1;
                }
            }
        }
    }

    private static void computeSubstract(String[] array) {
        for (int i = 0; i < array.length; i++) {
            if (array[i].equals("-")) {
                array[i - 1] = String.valueOf(substract(Double.parseDouble(array[i - 1]), Double.parseDouble(array[i + 1])));
                moveArrayForward(array, i + 2);
                if (i > 1) {
                    i = i - 2;
                } else if (i == 1) {
                    i = i - 1;
                }
            }
        }
    }

    private static void moveArrayForward(String[] array, int indexToMoveForward) {
        if (array.length - indexToMoveForward >= 0)
            System.arraycopy(array, indexToMoveForward, array, indexToMoveForward - 2, array.length - indexToMoveForward);

        array[array.length - 1] = "";
        array[array.length - 2] = "";
    }

    private static double add(double firstNumber, double secondNumber) {
        return firstNumber + secondNumber;
    }

    private static double substract(double firstNumber, double secondNumber) {
        return firstNumber - secondNumber;
    }

    private static double multiply(double firstNumber, double secondNumber) {
        return firstNumber * secondNumber;
    }

    private static double devide(double firstNumber, double secondNumber) {
        return firstNumber / secondNumber;
    }

}
