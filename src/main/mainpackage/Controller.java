package mainpackage;


import javafx.fxml.FXML;
import javafx.scene.control.Label;
import mainpackage.calculator.Calculator;
import mainpackage.checker.Checker;


public class Controller {
    //FIELDS
    @FXML
    private Label smallLabel, bigLabel;
    private String[] resultArray = null;
    private Boolean checkIfSummaryWasPressed = false;

    //CONSTRUCTOR
    public Controller() {
    }

    //START METHOD
    @FXML
    void initialize() {
    }

    //SUPPORTIVE METHODS
    private void charClicked(String number) {
        if (checkIfSummaryWasPressed) {
            bigLabel.setText(number);
            checkIfSummaryWasPressed = false;
        } else {
            bigLabel.setText(bigLabel.getText() + number);
        }
    }

    private boolean checkIfBigLabelDoesntContainMathematicalSymbol(){
        return (!bigLabel.getText().endsWith("+") && !bigLabel.getText().endsWith("-") && !bigLabel.getText().endsWith("*") && !bigLabel.getText().endsWith("/"));
    }

    private boolean checkIfBigLabelDoesntStartsWithMathematicalSymbol(){
        return (!bigLabel.getText().startsWith(" +") && !bigLabel.getText().startsWith(" -") && !bigLabel.getText().startsWith(" *") && !bigLabel.getText().startsWith(" /"));
    }
    private void computeResultwithEmptySmallLabel (){
        resultArray = bigLabel.getText().split(" ");
        String result = Calculator.computeResult(resultArray);
        smallLabel.setText(result);
        checkIfSummaryWasPressed = true;
        bigLabel.setText("");
    }

    private void computeResultWithPreviousResultOnSmallLabel(){
        bigLabel.setText(bigLabel.getText().replace(" +", "+")
                .replace(" -", "-")
                .replace(" *", "*")
                .replace(" /", "/"));
        resultArray = bigLabel.getText().split(" ");
        resultArray = Calculator.mixSmallLabelAndBigLabelToArray(smallLabel.getText(), resultArray);
        String result = Calculator.computeResult(resultArray);
        smallLabel.setText(result);
        checkIfSummaryWasPressed = true;
        bigLabel.setText("");
    }

    //FXML METHODS
    public void butt0OnAction() {
        charClicked("0");
    }

    public void butt1OnAction() {
        charClicked("1");
    }

    public void butt2OnAction() {
        charClicked("2");
    }

    public void butt3OnAction() {
        charClicked("3");
    }

    public void butt4OnAction() {
        charClicked("4");
    }

    public void butt5OnAction() {
        charClicked("5");
    }

    public void butt6OnAction() {
        charClicked("6");
    }

    public void butt7OnAction() {
        charClicked("7");
    }

    public void butt8OnAction() {
        charClicked("8");
    }

    public void butt9OnAction() {
        charClicked("9");
    }

    public void buttPlusOnAction() {
        charClicked(" + ");
    }

    public void buttSubstractOnAction() {
        charClicked(" - ");
    }

    public void buttMultiplyOnAction() {
        charClicked(" * ");
    }

    public void buttDevideOnAction() {
        charClicked(" / ");
    }

    public void buttCOnAction() {
        bigLabel.setText("");
        smallLabel.setText("");
    }

    public void buttComaOnAction() {
        if (!bigLabel.getText().contains(".")) {
            bigLabel.setText(bigLabel.getText() + ".");
        }
    }

    public void buttPMOnAction() {
        double number = Double.parseDouble(bigLabel.getText());
        number *= -1;
        bigLabel.setText(String.valueOf(number));
    }

    public void buttSummaryOnAction() {
        if (checkIfBigLabelDoesntContainMathematicalSymbol()) {
            if (checkIfBigLabelDoesntStartsWithMathematicalSymbol()) {
                computeResultwithEmptySmallLabel();
            } else {
                if (Checker.checkIfStringIsNumber(smallLabel.getText())) {
                    computeResultWithPreviousResultOnSmallLabel();
                }
            }
        }
    }
}